"use strict";
module.exports = function(sequelize, S) {
  var HealthcareService = sequelize.define('HealthcareService', {
    id: {
      type: S.UUID,
      defaultValue: S.UUIDV1,
      primaryKey: true,
      allowNull: false,
      comment: "Logical id of this artifact"
    },
    meta: {
      type: S.JSONB,
      allowNull: true,
      comment: "Metadata about the resource"
    },
    implicitRules: {
      type: S.TEXT,
      allowNull: true,
      comment: "A set of rules under which this content was created"
    },
    language: {
      type: S.TEXT,
      allowNull: true,
      comment: "Language of the resource content"
    },
    text: {
      type: S.JSONB,
      allowNull: true,
      comment: "Text summary of the resource, for human interpretation"
    },
    contained: {
      type: S.JSONB,
      allowNull: true,
      comment: "Contained, inline Resources"
    },
    extension: {
      type: S.JSONB,
      allowNull: true,
      comment: "Additional Content defined by implementations"
    },
    modifierExtension: {
      type: S.JSONB,
      allowNull: true,
      comment: "Extensions that cannot be ignored"
    },
    identifier: {
      type: S.JSONB,
      allowNull: true,
      comment: "External identifiers for this item"
    },
    providedBy: {
      type: S.JSONB,
      allowNull: true,
      comment: "Organization that provides this service"
    },
    serviceCategory: {
      type: S.JSONB,
      allowNull: true,
      comment: "Broad category of service being performed or delivered"
    },
    serviceType: {
      type: S.JSONB,
      allowNull: true,
      comment: "Specific service delivered or performed"
    },
    location: {
      type: S.JSONB,
      allowNull: false,
      comment: "Location where service may be provided"
    },
    serviceName: {
      type: S.TEXT,
      allowNull: true,
      comment: "Description of service as presented to a consumer while searching"
    },
    comment: {
      type: S.TEXT,
      allowNull: true,
      comment: "Additional description and/or any specific issues not covered elsewhere"
    },
    extraDetails: {
      type: S.TEXT,
      allowNull: true,
      comment: "Extra details about the service that can't be placed in the other fields"
    },
    photo: {
      type: S.JSONB,
      allowNull: true,
      comment: "Facilitates quick identification of the service"
    },
    telecom: {
      type: S.JSONB,
      allowNull: true,
      comment: "Contacts related to the healthcare service"
    },
    coverageArea: {
      type: S.JSONB,
      allowNull: true,
      comment: "Location(s) service is inteded for/available to"
    },
    serviceProvisionCode: {
      type: S.JSONB,
      allowNull: true,
      comment: "Conditions under which service is available/offered"
    },
    eligibility: {
      type: S.JSONB,
      allowNull: true,
      comment: "Specific eligibility requirements required to use the service"
    },
    eligibilityNote: {
      type: S.TEXT,
      allowNull: true,
      comment: "Describes the eligibility conditions for the service"
    },
    programName: {
      type: S.JSONB,
      allowNull: true,
      comment: "Program Names that categorize the service"
    },
    characteristic: {
      type: S.JSONB,
      allowNull: true,
      comment: "Collection of characteristics (attributes)"
    },
    referralMethod: {
      type: S.JSONB,
      allowNull: true,
      comment: "Ways that the service accepts referrals"
    },
    publicKey: {
      type: S.TEXT,
      allowNull: true,
      comment: "PKI Public keys to support secure communications"
    },
    appointmentRequired: {
      type: S.BOOLEAN,
      allowNull: true,
      comment: "If an appointment is required for access to this service"
    },
    availableTime: {
      type: S.JSONB,
      allowNull: true,
      comment: "Times the Service Site is available"
    },
    notAvailable: {
      type: S.JSONB,
      allowNull: true,
      comment: "Not available during this time due to provided reason"
    },
    availabilityExceptions: {
      type: S.TEXT,
      allowNull: true,
      comment: "Description of availability exceptions"
    }
  }, {
    tableName: 'healthcareService',
    paranoid: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
      // HealthcareService.belongsTo(models.User);
      }
    }
  });
  return HealthcareService;
};