"use strict";
module.exports = function(sequelize, S) {
  var Goal = sequelize.define('Goal', {
    id: {
      type: S.UUID,
      defaultValue: S.UUIDV1,
      primaryKey: true,
      allowNull: false,
      comment: "Logical id of this artifact"
    },
    meta: {
      type: S.JSONB,
      allowNull: true,
      comment: "Metadata about the resource"
    },
    implicitRules: {
      type: S.TEXT,
      allowNull: true,
      comment: "A set of rules under which this content was created"
    },
    language: {
      type: S.TEXT,
      allowNull: true,
      comment: "Language of the resource content"
    },
    text: {
      type: S.JSONB,
      allowNull: true,
      comment: "Text summary of the resource, for human interpretation"
    },
    contained: {
      type: S.JSONB,
      allowNull: true,
      comment: "Contained, inline Resources"
    },
    extension: {
      type: S.JSONB,
      allowNull: true,
      comment: "Additional Content defined by implementations"
    },
    modifierExtension: {
      type: S.JSONB,
      allowNull: true,
      comment: "Extensions that cannot be ignored"
    },
    identifier: {
      type: S.JSONB,
      allowNull: true,
      comment: "External Ids for this goal"
    },
    subject: {
      type: S.JSONB,
      allowNull: true,
      comment: "Who this goal is intended for"
    },
    start: {
      type: S.JSONB,
      allowNull: true,
      comment: "When goal pursuit begins"
    },
    target: {
      type: S.JSONB,
      allowNull: true,
      comment: "Reach goal on or before"
    },
    category: {
      type: S.JSONB,
      allowNull: true,
      comment: "E.g. Treatment, dietary, behavioral, etc."
    },
    description: {
      type: S.TEXT,
      allowNull: false,
      comment: "What's the desired outcome?"
    },
    status: {
      type: S.ENUM("proposed", "planned", "accepted", "rejected", "in-progress", "achieved", "sustaining", "on-hold", "cancelled"),
      allowNull: false,
      comment: "proposed | planned | accepted | rejected | in-progress | achieved | sustaining | on-hold | cancelled"
    },
    statusDate: {
      type: S.DATEONLY,
      allowNull: true,
      comment: "When goal status took effect"
    },
    statusReason: {
      type: S.JSONB,
      allowNull: true,
      comment: "Reason for current status"
    },
    author: {
      type: S.JSONB,
      allowNull: true,
      comment: "Who's responsible for creating Goal?"
    },
    priority: {
      type: S.ENUM("high", "medium", "low"),
      allowNull: true,
      comment: "high | medium | low"
    },
    addresses: {
      type: S.JSONB,
      allowNull: true,
      comment: "Issues addressed by this goal"
    },
    note: {
      type: S.JSONB,
      allowNull: true,
      comment: "Comments about the goal"
    },
    outcome: {
      type: S.JSONB,
      allowNull: true,
      comment: "What was end result of goal?"
    }
  }, {
    tableName: 'goal',
    paranoid: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
      // Goal.belongsTo(models.User);
      }
    }
  });
  return Goal;
};
