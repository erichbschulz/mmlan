'use strict';
module.exports = function(sequelize, S) {

  var User = sequelize.define('User', {
    id: {
      type: S.BIGINT,
      primaryKey: true,
      comment: "Staff ID",
    },
    name: {
      type: S.STRING,
      allowNull: false,
    },
    role: {
      type: S.ENUM('user', 'admin'),
      defaultValue: 'user',
      allowNull: false,
    }
  }, {
    tableName: 'user',
    paranoid: true,
//    classMethods: {
//      associate: function(models) {
//        User.hasMany(models.Event, {as: 'user_id'});
//      }
//    }
  });

  return User;

};
