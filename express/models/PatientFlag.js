"use strict";
module.exports = function(sequelize, S) {
  var PatientFlag = sequelize.define('PatientFlag', {
    id: {
      type: S.UUID,
      defaultValue: S.UUIDV1,
      primaryKey: true,
      allowNull: false,
      comment: "Logical id of this artifact"
    },
    category: {
      type: S.ENUM("diet", "drug", "lab", "admin", "contact", "medical"),
      allowNull: false,
      comment: "Clinical, administrative, etc."
    },
    status: {
      type: S.ENUM("active", "inactive", "entered-in-error"),
      allowNull: false,
      comment: "active | inactive | entered-in-error"
    },
    start: {
      type: S.JSONB,
      allowNull: true,
      comment: "Time period when flag is active"
    },
    end: {
      type: S.JSONB,
      allowNull: true,
      comment: "Time period when flag is active"
    },
    encounter: {
      type: S.JSONB,
      allowNull: true,
      comment: "Alert relevant during encounter"
    },
    author: {
      type: S.JSONB,
      allowNull: true,
      comment: "Flag creator"
    },
    code: {
      type: S.TEXT,
      allowNull: false,
      comment: "Partially deaf, Requires easy open caps, No permanent address, etc."
    },
    text: {
      type: S.TEXT,
      allowNull: false,
      comment: "Partially deaf, Requires easy open caps, No permanent address, etc."
    },
  }, {
    tableName: 'Patientflag',
    paranoid: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
      PatientFlag.belongsTo(models.Patient);
      }
    }
  });
  return PatientFlag;
};
