"use strict";
module.exports = function(sequelize, S) {
  var FamilyMemberHistory = sequelize.define('FamilyMemberHistory', {
    id: {
      type: S.UUID,
      defaultValue: S.UUIDV1,
      primaryKey: true,
      allowNull: false,
      comment: "Logical id of this artifact"
    },
    date: {
      type: S.DATE, defaultValue: S.NOW,
      allowNull: true,
      comment: "When history was captured/updated"
    },
    status: {
      type: S.ENUM("partial", "completed", "entered-in-error", "health-unknown"),
      allowNull: false,
      comment: "partial | completed | entered-in-error | health-unknown"
    },
    name: {
      type: S.TEXT,
      allowNull: true,
      comment: "The family member described"
    },
    relationship: {
      type: S.JSONB,
      allowNull: false,
      comment: "Relationship to the subject"
    },
    gender: {
      type: S.ENUM("male", "female", "other", "unknown"),
      allowNull: false,
      comment: "male | female | other | unknown"
    },
    born: {
      type: S.JSONB,
      allowNull: true,
      comment: "(approximate) date of birth"
    },
    age: {
      type: S.JSONB,
      allowNull: true,
      comment: "(approximate) age"
    },
    deceased: {
      type: S.JSONB,
      allowNull: true,
      comment: "Dead? How old/when?"
    },
    condition: {
      type: S.JSONB,
      allowNull: true,
      comment: "Condition that the related person had"
    }
  }, {
    tableName: 'familyMemberHistory',
    paranoid: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
        FamilyMemberHistory.belongsTo(models.Patient);
        FamilyMemberHistory.belongsTo(models.Event);
      }
    }
  });
  return FamilyMemberHistory;
};
