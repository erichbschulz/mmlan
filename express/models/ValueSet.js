"use strict";
module.exports = function(sequelize, S) {
  var ValueSet = sequelize.define('ValueSet', {
    id: {
      type: S.UUID,
      defaultValue: S.UUIDV1,
      primaryKey: true,
      allowNull: false,
      comment: "Logical id of this artifact"
    },
    meta: {
      type: S.JSONB,
      allowNull: true,
      comment: "Metadata about the resource"
    },
    implicitRules: {
      type: S.TEXT,
      allowNull: true,
      comment: "A set of rules under which this content was created"
    },
    language: {
      type: S.TEXT,
      allowNull: true,
      comment: "Language of the resource content"
    },
    text: {
      type: S.JSONB,
      allowNull: true,
      comment: "Text summary of the resource, for human interpretation"
    },
    contained: {
      type: S.JSONB,
      allowNull: true,
      comment: "Contained, inline Resources"
    },
    extension: {
      type: S.JSONB,
      allowNull: true,
      comment: "Additional Content defined by implementations"
    },
    modifierExtension: {
      type: S.JSONB,
      allowNull: true,
      comment: "Extensions that cannot be ignored"
    },
    url: {
      type: S.TEXT,
      allowNull: true,
      comment: "Globally unique logical identifier for  value set"
    },
    identifier: {
      type: S.JSONB,
      allowNull: true,
      comment: "Additional identifier for the value set (e.g. HL7 v2 / CDA)"
    },
    version: {
      type: S.TEXT,
      allowNull: true,
      comment: "Logical identifier for this version of the value set"
    },
    name: {
      type: S.TEXT,
      allowNull: true,
      comment: "Informal name for this value set"
    },
    status: {
      type: S.TEXT,
      allowNull: false,
      comment: "draft | active | retired"
    },
    experimental: {
      type: S.BOOLEAN,
      allowNull: true,
      comment: "If for testing purposes, not real usage"
    },
    publisher: {
      type: S.TEXT,
      allowNull: true,
      comment: "Name of the publisher (organization or individual)"
    },
    contact: {
      type: S.JSONB,
      allowNull: true,
      comment: "Contact details of the publisher"
    },
    date: {
      type: S.DATE, defaultValue: S.NOW,
      allowNull: true,
      comment: "Date for given status"
    },
    lockedDate: {
      type: S.DATEONLY,
      allowNull: true,
      comment: "Fixed date for all referenced code systems and value sets"
    },
    description: {
      type: S.TEXT,
      allowNull: true,
      comment: "Human language description of the value set"
    },
    useContext: {
      type: S.JSONB,
      allowNull: true,
      comment: "Content intends to support these contexts"
    },
    immutable: {
      type: S.BOOLEAN,
      allowNull: true,
      comment: "Indicates whether or not any change to the content logical definition may occur"
    },
    requirements: {
      type: S.TEXT,
      allowNull: true,
      comment: "Why needed"
    },
    copyright: {
      type: S.TEXT,
      allowNull: true,
      comment: "Use and/or publishing restrictions"
    },
    extensible: {
      type: S.BOOLEAN,
      allowNull: true,
      comment: "Whether this is intended to be used with an extensible binding"
    },
    codeSystem: {
      type: S.JSONB,
      allowNull: true,
      comment: "An inline code system, which is part of this value set"
    },
    compose: {
      type: S.JSONB,
      allowNull: true,
      comment: "When value set includes codes from elsewhere"
    },
    expansion: {
      type: S.JSONB,
      allowNull: true,
      comment: "Used when the value set is \"expanded\""
    }
  }, {
    tableName: 'valueSet',
    paranoid: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
      // ValueSet.belongsTo(models.User);
      }
    }
  });
  return ValueSet;
};