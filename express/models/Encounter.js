"use strict";
module.exports = function(sequelize, S) {
  var Encounter = sequelize.define('Encounter', {
    id: {
      type: S.UUID,
      defaultValue: S.UUIDV1,
      primaryKey: true,
      allowNull: false,
      comment: "Logical id of this artifact"
    },
    status: {
      type: S.ENUM('planned', 'arrived', 'in-progress', 'onleave', 'finished',
              'cancelled'),
      allowNull: false,
      comment: "planned | arrived | in-progress | onleave | finished | cancelled"
    },
    statusHistory: {
      type: S.JSONB,
      allowNull: true,
      comment: "List of past encounter statuses"
    },
    class: {
      type: S.ENUM('inpatient', 'outpatient', 'ambulatory', 'emergency',
              'virtual'),
      allowNull: true,
      comment: "inpatient | outpatient | ambulatory | emergency"
    },
    type: {
      type: S.JSONB,
      allowNull: true,
      comment: "Specific type of encounter - eg APS assessment"
    },
    priority: {
      type: S.TEXT,
      allowNull: true,
      comment: "Indicates the urgency of the encounter (enum?)"
    },
    episodeOfCare: {
      type: S.UUID,
      allowNull: true,
      comment: "Episode(s) of care that this encounter should be recorded against"
    },
    incomingReferral: {
      type: S.UUID,
      allowNull: true,
      comment: "The ReferralRequest that initiated this encounter"
    },
    participant: {
      type: S.JSONB,
      allowNull: true,
      comment: "List of participants involved in the encounter"
    },
    appointment: {
      type: S.UUID,
      allowNull: true,
      comment: "The appointment that scheduled this encounter"
    },
    period: {
      type: S.JSONB,
      allowNull: true,
      comment: "The start and end time of the encounter"
    },
    length: {
      type: S.INTEGER,
      allowNull: true,
      comment: "Quantity of time the encounter lasted (less time absent)"
    },
    reason: {
      type: S.JSONB,
      allowNull: true,
      comment: "Reason the encounter takes place (code)"
    },
    indication: {
      type: S.JSONB,
      allowNull: true,
      comment: "Reason the encounter takes place (resource)"
    },
    hospitalization: {
      type: S.JSONB,
      allowNull: true,
      comment: "Details about the admission to a healthcare service"
    },
    location: {
      type: S.JSONB,
      allowNull: true,
      comment: "List of locations where the patient has been"
    },
    serviceProvider: {
      type: S.JSONB,
      allowNull: true,
      comment: "The custodian organization of this Encounter record"
    },
    partOf: {
      type: S.JSONB,
      allowNull: true,
      comment: "Another Encounter this encounter is part of"
    }
  }, {
    tableName: 'encounter',
    paranoid: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
        Encounter.belongsTo(models.Patient);
      }
    }
  });
  return Encounter;
};
