"use strict";
module.exports = function(sequelize, S) {
  var Practitioner = sequelize.define('Practitioner', {
    id: {
      type: S.BIGINT.UNSIGNED,
      primaryKey: true,
      allowNull: false,
      comment: "Logical id of this artifact"
    },
    meta: {
      type: S.JSONB,
      allowNull: true,
      comment: "Metadata about the resource"
    },
    implicitRules: {
      type: S.TEXT,
      allowNull: true,
      comment: "A set of rules under which this content was created"
    },
    language: {
      type: S.TEXT,
      allowNull: true,
      comment: "Language of the resource content"
    },
    text: {
      type: S.JSONB,
      allowNull: true,
      comment: "Text summary of the resource, for human interpretation"
    },
    contained: {
      type: S.JSONB,
      allowNull: true,
      comment: "Contained, inline Resources"
    },
    extension: {
      type: S.JSONB,
      allowNull: true,
      comment: "Additional Content defined by implementations"
    },
    modifierExtension: {
      type: S.JSONB,
      allowNull: true,
      comment: "Extensions that cannot be ignored"
    },
    identifier: {
      type: S.JSONB,
      allowNull: true,
      comment: "A identifier for the person as this agent"
    },
    active: {
      type: S.BOOLEAN,
      allowNull: true,
      comment: "Whether this practitioner's record is in active use"
    },
    name: {
      type: S.JSONB,
      allowNull: true,
      comment: "A name associated with the person"
    },
    telecom: {
      type: S.JSONB,
      allowNull: true,
      comment: "A contact detail for the practitioner"
    },
    address: {
      type: S.JSONB,
      allowNull: true,
      comment: "Where practitioner can be found/visited"
    },
    gender: {
      type: S.TEXT,
      allowNull: true,
      comment: "male | female | other | unknown"
    },
    birthDate: {
      type: S.DATEONLY,
      allowNull: true,
      comment: "The date  on which the practitioner was born"
    },
    photo: {
      type: S.JSONB,
      allowNull: true,
      comment: "Image of the person"
    },
    practitionerRole: {
      type: S.JSONB,
      allowNull: true,
      comment: "Roles/organizations the practitioner is associated with"
    },
    qualification: {
      type: S.JSONB,
      allowNull: true,
      comment: "Qualifications obtained by training and certification"
    },
    communication: {
      type: S.JSONB,
      allowNull: true,
      comment: "A language the practitioner is able to use in patient communication"
    }
  }, {
    tableName: 'practitioner',
    paranoid: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
      // Practitioner.belongsTo(models.User);
      }
    }
  });
  return Practitioner;
};