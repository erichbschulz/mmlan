"use strict";
module.exports = function(sequelize, S) {
  var Location = sequelize.define('Location', {
    id: {
      type: S.UUID,
      defaultValue: S.UUIDV1,
      primaryKey: true,
      allowNull: false,
      comment: "Logical id of this artifact"
    },
    meta: {
      type: S.JSONB,
      allowNull: true,
      comment: "Metadata about the resource"
    },
    implicitRules: {
      type: S.TEXT,
      allowNull: true,
      comment: "A set of rules under which this content was created"
    },
    language: {
      type: S.TEXT,
      allowNull: true,
      comment: "Language of the resource content"
    },
    text: {
      type: S.JSONB,
      allowNull: true,
      comment: "Text summary of the resource, for human interpretation"
    },
    contained: {
      type: S.JSONB,
      allowNull: true,
      comment: "Contained, inline Resources"
    },
    extension: {
      type: S.JSONB,
      allowNull: true,
      comment: "Additional Content defined by implementations"
    },
    modifierExtension: {
      type: S.JSONB,
      allowNull: true,
      comment: "Extensions that cannot be ignored"
    },
    identifier: {
      type: S.JSONB,
      allowNull: true,
      comment: "Unique code or number identifying the location to its users"
    },
    status: {
      type: S.TEXT,
      allowNull: true,
      comment: "active | suspended | inactive"
    },
    name: {
      type: S.TEXT,
      allowNull: true,
      comment: "Name of the location as used by humans"
    },
    description: {
      type: S.TEXT,
      allowNull: true,
      comment: "Description of the location"
    },
    mode: {
      type: S.TEXT,
      allowNull: true,
      comment: "instance | kind"
    },
    type: {
      type: S.JSONB,
      allowNull: true,
      comment: "Type of function performed"
    },
    telecom: {
      type: S.JSONB,
      allowNull: true,
      comment: "Contact details of the location"
    },
    address: {
      type: S.JSONB,
      allowNull: true,
      comment: "Physical location"
    },
    physicalType: {
      type: S.JSONB,
      allowNull: true,
      comment: "Physical form of the location"
    },
    position: {
      type: S.JSONB,
      allowNull: true,
      comment: "The absolute geographic location"
    },
    managingOrganization: {
      type: S.JSONB,
      allowNull: true,
      comment: "Organization responsible for provisioning and upkeep"
    },
    partOf: {
      type: S.JSONB,
      allowNull: true,
      comment: "Another Location this one is physically part of"
    }
  }, {
    tableName: 'location',
    paranoid: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
      // Location.belongsTo(models.User);
      }
    }
  });
  return Location;
};