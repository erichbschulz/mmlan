"use strict";
module.exports = function(sequelize, S) {
  var Questionnaire = sequelize.define('Questionnaire', {
    id: {
      type: S.UUID,
      defaultValue: S.UUIDV1,
      primaryKey: true,
      allowNull: false,
      comment: "Logical id of this artifact"
    },
    meta: {
      type: S.JSONB,
      allowNull: true,
      comment: "Metadata about the resource"
    },
    implicitRules: {
      type: S.TEXT,
      allowNull: true,
      comment: "A set of rules under which this content was created"
    },
    language: {
      type: S.TEXT,
      allowNull: true,
      comment: "Language of the resource content"
    },
    text: {
      type: S.JSONB,
      allowNull: true,
      comment: "Text summary of the resource, for human interpretation"
    },
    contained: {
      type: S.JSONB,
      allowNull: true,
      comment: "Contained, inline Resources"
    },
    extension: {
      type: S.JSONB,
      allowNull: true,
      comment: "Additional Content defined by implementations"
    },
    modifierExtension: {
      type: S.JSONB,
      allowNull: true,
      comment: "Extensions that cannot be ignored"
    },
    identifier: {
      type: S.JSONB,
      allowNull: true,
      comment: "External identifiers for this questionnaire"
    },
    version: {
      type: S.TEXT,
      allowNull: true,
      comment: "Logical identifier for this version of Questionnaire"
    },
    status: {
      type: S.TEXT,
      allowNull: false,
      comment: "draft | published | retired"
    },
    date: {
      type: S.DATE, defaultValue: S.NOW,
      allowNull: true,
      comment: "Date this version was authored"
    },
    publisher: {
      type: S.TEXT,
      allowNull: true,
      comment: "Organization/individual who designed the questionnaire"
    },
    telecom: {
      type: S.JSONB,
      allowNull: true,
      comment: "Contact information of the publisher"
    },
    subjectType: {
      type: S.JSONB,
      allowNull: true,
      comment: "Resource that can be subject of QuestionnaireResponse"
    },
    group: {
      type: S.JSONB,
      allowNull: false,
      comment: "Grouped questions"
    }
  }, {
    tableName: 'questionnaire',
    paranoid: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
      // Questionnaire.belongsTo(models.User);
      }
    }
  });
  return Questionnaire;
};