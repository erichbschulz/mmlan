"use strict";
module.exports = function(sequelize, S) {
  var Procedure = sequelize.define('Procedure', {
    id: {
      type: S.UUID,
      defaultValue: S.UUIDV1,
      primaryKey: true,
      allowNull: false,
      comment: "Logical id of this artifact"
    },
    meta: {
      type: S.JSONB,
      allowNull: true,
      comment: "Metadata about the resource"
    },
    implicitRules: {
      type: S.TEXT,
      allowNull: true,
      comment: "A set of rules under which this content was created"
    },
    language: {
      type: S.TEXT,
      allowNull: true,
      comment: "Language of the resource content"
    },
    text: {
      type: S.JSONB,
      allowNull: true,
      comment: "Text summary of the resource, for human interpretation"
    },
    contained: {
      type: S.JSONB,
      allowNull: true,
      comment: "Contained, inline Resources"
    },
    extension: {
      type: S.JSONB,
      allowNull: true,
      comment: "Additional Content defined by implementations"
    },
    modifierExtension: {
      type: S.JSONB,
      allowNull: true,
      comment: "Extensions that cannot be ignored"
    },
    identifier: {
      type: S.JSONB,
      allowNull: true,
      comment: "External Identifiers for this procedure"
    },
    subject: {
      type: S.JSONB,
      allowNull: false,
      comment: "Who the procedure was performed on"
    },
    status: {
      type: S.ENUM("in-progress", "aborted", "completed", "entered-in-error"),
      allowNull: false,
      comment: "in-progress | aborted | completed | entered-in-error"
    },
    category: {
      type: S.JSONB,
      allowNull: true,
      comment: "Classification of the procedure"
    },
    code: {
      type: S.JSONB,
      allowNull: false,
      comment: "Identification of the procedure"
    },
    notPerformed: {
      type: S.BOOLEAN,
      allowNull: true,
      comment: "True if procedure was not performed as scheduled"
    },
    reasonNotPerformed: {
      type: S.JSONB,
      allowNull: true,
      comment: "Reason procedure was not performed"
    },
    bodySite: {
      type: S.JSONB,
      allowNull: true,
      comment: "Target body sites"
    },
    reason: {
      type: S.JSONB,
      allowNull: true,
      comment: "Reason procedure performed"
    },
    performer: {
      type: S.JSONB,
      allowNull: true,
      comment: "The people who performed the procedure"
    },
    performed: {
      type: S.JSONB,
      allowNull: true,
      comment: "Date/Period the procedure was performed"
    },
    encounter: {
      type: S.JSONB,
      allowNull: true,
      comment: "The encounter associated with the procedure"
    },
    location: {
      type: S.JSONB,
      allowNull: true,
      comment: "Where the procedure happened"
    },
    outcome: {
      type: S.JSONB,
      allowNull: true,
      comment: "The result of procedure"
    },
    report: {
      type: S.JSONB,
      allowNull: true,
      comment: "Any report resulting from the procedure"
    },
    complication: {
      type: S.JSONB,
      allowNull: true,
      comment: "Complication following the procedure"
    },
    followUp: {
      type: S.JSONB,
      allowNull: true,
      comment: "Instructions for follow up"
    },
    request: {
      type: S.JSONB,
      allowNull: true,
      comment: "A request for this procedure"
    },
    notes: {
      type: S.JSONB,
      allowNull: true,
      comment: "Additional information about the procedure"
    },
    focalDevice: {
      type: S.JSONB,
      allowNull: true,
      comment: "Device changed in procedure"
    },
    used: {
      type: S.JSONB,
      allowNull: true,
      comment: "Items used during procedure"
    }
  }, {
    tableName: 'procedure',
    paranoid: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
      // Procedure.belongsTo(models.User);
      }
    }
  });
  return Procedure;
};
