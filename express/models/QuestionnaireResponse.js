"use strict";
module.exports = function(sequelize, S) {
  var QuestionnaireResponse = sequelize.define('QuestionnaireResponse', {
    id: {
      type: S.UUID,
      defaultValue: S.UUIDV1,
      primaryKey: true,
      allowNull: false,
      comment: "Logical id of this artifact"
    },
    meta: {
      type: S.JSONB,
      allowNull: true,
      comment: "Metadata about the resource"
    },
    implicitRules: {
      type: S.TEXT,
      allowNull: true,
      comment: "A set of rules under which this content was created"
    },
    language: {
      type: S.TEXT,
      allowNull: true,
      comment: "Language of the resource content"
    },
    text: {
      type: S.JSONB,
      allowNull: true,
      comment: "Text summary of the resource, for human interpretation"
    },
    contained: {
      type: S.JSONB,
      allowNull: true,
      comment: "Contained, inline Resources"
    },
    extension: {
      type: S.JSONB,
      allowNull: true,
      comment: "Additional Content defined by implementations"
    },
    modifierExtension: {
      type: S.JSONB,
      allowNull: true,
      comment: "Extensions that cannot be ignored"
    },
    identifier: {
      type: S.JSONB,
      allowNull: true,
      comment: "Unique id for this set of answers"
    },
    questionnaire: {
      type: S.JSONB,
      allowNull: true,
      comment: "Form being answered"
    },
    status: {
      type: S.TEXT,
      allowNull: false,
      comment: "in-progress | completed | amended"
    },
    subject: {
      type: S.JSONB,
      allowNull: true,
      comment: "The subject of the questions"
    },
    author: {
      type: S.JSONB,
      allowNull: true,
      comment: "Person who received and recorded the answers"
    },
    authored: {
      type: S.DATE, defaultValue: S.NOW,
      allowNull: true,
      comment: "Date this version was authored"
    },
    source: {
      type: S.JSONB,
      allowNull: true,
      comment: "The person who answered the questions"
    },
    encounter: {
      type: S.JSONB,
      allowNull: true,
      comment: "Primary encounter during which the answers were collected"
    },
    group: {
      type: S.JSONB,
      allowNull: true,
      comment: "Grouped questions"
    }
  }, {
    tableName: 'questionnaireResponse',
    paranoid: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
      // QuestionnaireResponse.belongsTo(models.User);
      }
    }
  });
  return QuestionnaireResponse;
};