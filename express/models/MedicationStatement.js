"use strict";
module.exports = function(sequelize, S) {
  var MedicationStatement = sequelize.define('MedicationStatement', {
    id: {
      type: S.UUID,
      defaultValue: S.UUIDV1,
      primaryKey: true,
      allowNull: false,
      comment: "Logical id of this artifact"
    },
    text: {
      type: S.JSONB,
      allowNull: true,
      comment: "Text summary of the resource, for human interpretation"
    },
    informationSource: {
      type: S.JSONB,
      allowNull: true,
      comment: undefined
    },
    dateAsserted: {
      type: S.DATE, defaultValue: S.NOW,
      allowNull: true,
      comment: "When the statement was asserted?"
    },
    status: {
      type: S.ENUM("active", "completed", "entered-in-error", "intended"),
      allowNull: false,
      comment: "active | completed | entered-in-error | intended"
    },
    wasNotTaken: {
      type: S.BOOLEAN,
      allowNull: false,
      defaultValue: false,
      comment: "True if medication is/was not being taken"
    },
    reasonNotTaken: {
      type: S.JSONB,
      allowNull: true,
      comment: "True if asserting medication was not given"
    },
    reasonForUse: {
      type: S.JSONB,
      allowNull: true,
      comment: undefined
    },
    start: {
      type: S.JSONB,
      allowNull: true,
      comment: "Over what period was medication consumed?"
    },
    end: {
      type: S.JSONB,
      allowNull: true,
      comment: "Over what period was medication consumed?"
    },
    note: {
      type: S.TEXT,
      allowNull: true,
      comment: "Further information about the statement"
    },
    supportingInformation: {
      type: S.JSONB,
      allowNull: true,
      comment: "Additional supporting information"
    },
    medication: {
      type: S.JSONB,
      allowNull: false,
      comment: "What medication was taken"
    },
    dosage: {
      type: S.JSONB,
      allowNull: true,
      comment: "Details of how medication was taken"
    },
    route: {
      type: S.JSONB,
      allowNull: true,
      comment: ""
    },
    site: {
      type: S.JSONB,
      allowNull: true,
      comment: ""
    },
    method: {
      type: S.JSONB,
      allowNull: true,
      comment: ""
    },
  }, {
    tableName: 'medicationStatement',
    paranoid: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
        MedicationStatement.belongsTo(models.Patient);
      }
    }
  });
  return MedicationStatement;
};
