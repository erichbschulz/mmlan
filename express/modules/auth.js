"use strict";
var Promise = require('bluebird');
var request = require('request-promise'); // bluebird wrapper around request
var moment = require('moment'); // bluebird wrapper around moment

module.exports = function(config) {
  var auth = {};
  var server = config.MML_API;

  auth.hasRole = function(session, role) {
    return session.user && session.user.roles;
  }

  /**
    * Login function
    * @params
    * - session
    * - credentials
    *   - user
    *   - pass
    */
  auth.login = function(session, credentials) {
    var mocking = config.env == 'development' && server == "mock";
    // use the mocks only if server url is mock, and we are in dev mode
    var auth = mocking ? login_mock : mml_login;
    return auth(credentials)
    .then(function(user) {
      console.log('auth.login success', credentials.user);
      config.seq_log.info({user: credentials.user}, 'log-in');
      session.user = user;
      session.mocking = mocking;
      return user;
    });
  }

  /**
    * Login function mock
    * @params credentials
    *   - user
    *   - pass
    * @return promise of user object or throw on bad password
    */
  function login_mock(credentials) {
    return Promise.delay(10)
    .then(function() {
      console.log('mock login by credentials', credentials.user);
      if (credentials.user > 1000 && credentials.pass=='123') {
        var user = {
          name: "user " + credentials.user,
          id: credentials.user,
          token: 'aMockToken',
          roles: ["user"],
        };
        return user;
      }
      else {
        throw new Error('failed');
      }
    });
  }

  /**
    * Login function
    * @params credentials
    *   - user
    *   - pass
    * @return promise of user object or throw on bad password
    * - name string
    * - server string url of server
    * - id integer user id
    * - roles array of string eg ['user']
    * - token
    *    - access_token
    *    - expires
    */
  function mml_login(credentials) {
    var uri = server + "/Token";
    var options = {
      method: 'POST',
      headers: [{
        "X-TraceCorrelationActivityId": "watch this space"}],
      uri: uri,
      form: {
        grant_type: "password",
        username: credentials.user,
        password: credentials.pass
      },
      json: true
    };
    return request(options)
    .then(function(result) {
      var user = {
        server: server,
        name: "user " + credentials.user,
        id: credentials.user,
        roles: ["user", "me"], // fixme
        token: {
          access_token: result.access_token,
          issued: moment(result.issued),
          // these two are supplied by ignored:
          // expires_in: result.expires_in,  // integer
          //expires: moment(result.expires)
        }
      };
      return Promise.resolve(user);
    });
  }

  return auth;
}
