"use strict";
/**
  Utilities to interact with terminology services
  */
var Promise = require('bluebird');
var S = require('sequelize');
var _ = require('lodash');
var rp = require('request-promise'); // bluebird wrapper around request

module.exports = function(config) {

  // injected cofiguration variable
  var s = config.sequelize;
  var db = config.db;
  var server = "http://its.patientsfirst.org.nz/RestService.svc/Terminz";
  server = "http://ontoserver.csiro.au/fhir";
    // ValueSet/$expand?identifier=http://snomed.info/sct?fhir_vs=refset/142321000036106&count=10&filter=met


  // main export object
  var ontology = {};

  ontology.test = function() {
    var uri = server + "/ValueSet/$expand";
    var options = {
      method: 'GET',
      uri: uri,
      qs: {identifier: 'http://snomed.info/sct?fhir_vs=isa/38341003'},
      json: true // Automatically stringifies the body to JSON
    };
    return rp(options)
  };

  return ontology;

}
