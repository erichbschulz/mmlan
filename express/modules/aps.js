"use strict";
var Promise = require('bluebird');
var _ = require('lodash');
var moment = require('moment');
var request = require('request-promise'); // bluebird wrapper around request

var Aps = function(config) {
  var sys = config.sys;
  var fhir_tables = sys.fhir_tables;
  var aps = {};

  function next_round() {
    return moment().tz(config.time_zone)
      // start of tomorow, or today if before 10am
      .subtract(10).startOf('day').add(1, 'day')
      .hour(8);
  }

  /*
   * Receive new data on an APS patient
   * @params object
   * - action string refer|assess|discharge
   * - patient_id integer
   * - session object
   * - data object
   * @returns promise of object
   * - patient object (model)
   * - event object (model)
   */
  aps.receive = function(params) {
    var user_id = params.session.user.id;
    // array of functions to handle specific events
    var actions = {
      refer: recordReferral,
      discharge: recordDischarge,
      assess: recordAssessment
    };
    var details = params.data;
    // if sending an assessment with plan of discharge then treat as a
    // dischare event
    if (params.action == 'assess' && details.review_plan == 'discharge') {
      params.action = 'discharge';
    }
    // get the method for the action
    var action = actions[params.action];
    if (!action) {
      throw new Error('bad action ' + params.action);
    }
    // package up the event
    var event_rec = {
      user_id: user_id,
      patient_id: params.patient_id,
      details: details, // fixme need to strip unneeded noise?
    };
    // get patient and save details
    var promise = registerPatient(params).
      then(function (patient) {
        return Promise.props({
          patient: patient,
          event_rec: event_rec,
          user_id: user_id,
          received_params: details
        });
      })
      .then(action);
    return promise;
  };

  /*
   * Get a patient object if in APS
   * @param object session
   * @param integer patient_id
   * @returns promise of patient
   */
  aps.getApsRecord = function(session, patient_id) {
    var final_result;
    var model_promises = {};
    // loop over tables and get all the data
    // this is a massive load and will need filtering
    _.forEach(fhir_tables, function(model) {
      model_promises[model] = sys.models[model].findAll({
        where: {patient_id: patient_id}});
    });
    return Promise.props(_.extend(model_promises, {
      patient: getApsPatient(session, patient_id),
      events: sys.models.Event.findAll({
        where: {patient_id: patient_id}})
    }))
    .then(function(aps_record) { // get the status
      final_result = aps_record;
      return aps.patientStatus(aps_record);
    })
    .then(function(status) {
      final_result.status = status;
      return final_result;
    });
  };

  /*
   * Get a list of patients
   * @returns promise of array
   */
  aps.getPatients = function(session) {
    var promise = config.db.get("SELECT p.id, p.name, p.dob, e.start " +
        "FROM patient AS p INNER JOIN \"episodeOfCare\" AS e ON p.id = e.patient_id " +
        "WHERE p.\"deletedAt\" IS NULL AND e.status='active' AND e.type='APS' AND e.\"deleted_at\" IS NULL");
    return promise;
  };

  /*
   * Get a patient object if in APS
   * @param object session
   * @param integer patient_id
   * @returns promise of patient
   */
  var getApsPatient = function(session, patient_id) {
    return sys.models.Patient.findOne({where: {id: patient_id}});
  };

  /*
   * Get a patient alerts
   * @param object session
   * @param integer patient_id
   * @param string service
   * @returns promise of collection
   */
  var getPatientData = function(session, patient_id, service, params) {
    params = params || {};
    console.log('getting patient data', patient_id, service);
    if (session.mocking) {
      let mock = require('./mock');
      return mock.data(patient_id, service, params);
    }
    else {
      let options = {
        method: 'GET',
        uri: session.user.server + "/api/" + service,
        qs: _.extend({urNumber: patient_id}, params),
        json: true // Automatically stringifies the body to JSON
      };
      return getData(session, {
        options: options,
        task: service + " look up for " + patient_id
      });
    }
  };

  /*
   * Get a test service
   * @param object session
   * @param string service
   * @returns promise
   */
  var getTestService = function(session, service) {
    let options = {
      method: 'GET',
      uri: session.user.server + "/api/" + service,
      json: true // Automatically stringifies the body to JSON
    };
    return getData(session, {
      options: options,
      task: service + " test"
    });
  };

  /*
   * Requests data from RESTful server
   * @param session object
   * @param params object
   * - options
   * - task string description of task for logging and error
   * @returns promise or FHIR data-absent-reason POJO on failure
   */
  var getData = function(session, params) {
    // add the bearer token from the session:
    params.options.auth = {bearer: _.get(session, 'user.token.access_token')};
    return request(params.options)
    .catch(function(error) {
      config.seq_log.error(
        {error: error, task: params.task});
      return {
        extension: [{
          url : "http://hl7.org/fhir/StructureDefinition/data-absent-reason",
          valueCode: "error" ,
          error: 'failed ' + params.task}]};
    });
  };

  /*
   * Get a patient object if in APS
   * @param object session
   * @param integer patient_id
   * @returns promise of patient
   */
  aps.getPatient = function(session, patient_id) {
    return Promise.props({
      patient_id: patient_id,
      aps: getApsPatient(session, patient_id),
      patient: getPatientData(session, patient_id, 'unstructured/patient'),
      alerts: getPatientData(session, patient_id, 'unstructured/patientalerts'),
      currentepisode: getPatientData(session, patient_id, 'unstructured/patientcurrentepisode'),
      locations: getPatientData(session, patient_id, 'unstructured/patientlocations'),
      results: getPatientData(session, patient_id, 'unstructured/patientresults'),
      results1: getPatientData(session, patient_id, 'unstructured/patientresults',
          {fromDate: "2015-01-01", toDate: "2015-12-31"}),
      // get some test endpoints:
      ping: getTestService(session, 'ping'),
      pingError: getTestService(session, 'ping/Error'),
      AuthCheck: getTestService(session, 'ping/AuthCheck'),
    });
  };

  /*
   * Register a patient if not already in APS
   * @params object
   * - patient_id integer
   * - data object
   * - session object
   * @returns promise of patient object
   */
  var registerPatient = function(params) {
    var patient_promise = getApsPatient(params.session, params.patient_id).
    then(function(patient) {
      if (!patient) { // no patient so create
        var new_patient = {
          id: params.patient_id,
          name: params.data.name,
          dob: params.data.dob
          };
        return Patient.create(new_patient);
      }
      else {
        return patient;
      }
    });
    return patient_promise;
  };

  /*
   * Create a referral
   * @params object
   * - patient object (model)
   * - data object (rec)
   * @returns promise of object
   * - patient object (model)
   * - event object (model)
   */
  var recordReferral = function(params) {
    var accumulator = {
      params: params.received_params,
      user_id: params.user_id,
    };
    var event_rec = params.event_rec;
    var priority = event_rec.review_plan == 'review today'
      ? 'urgent' : 'routine';
    event_rec.type = 'APS_referral';
    var referral_rec = {
      patient_id: params.patient.id,
      requester: {user: params.user_id},
      status: 'active',
      category: 'request',
      type: 'provide-care',
      specialty: 'Acute-pain',
      description: event_rec.notes,
      priority: priority,
      basedOn: null,
      parent: null,
      context: null,
      fullfillmentStart: null,
      fullfillmentEnd: null,
      reason: null, // codable
      serviceRequested: null, // codable
      supportingInformation: null // references
    };
    return recordEvent(params.patient, event_rec)
    // create referral
    .then(function (event_result) {
      // gets patient and event
      _.extend(accumulator, event_result);
      referral_rec.basedOn = {event: event_result.event.id};
      return sys.models.ReferralRequest.create(referral_rec);
    })
    .then(function(ReferralRequest) {
      accumulator.referral = ReferralRequest;
      accumulator.source = 'ReferralRequest/' + ReferralRequest.id;
      // pass both therapies and conditions arrays through respective handlers
      return Promise.props({
        conditions: Promise.map(params.received_params.conditions,
          function(condition) {
            return recordCondition(condition, accumulator);
          }),
        therapies: Promise.map(params.received_params.therapies,
          function(therapy) {
            return recordTherapy(therapy, accumulator);
          }),
      });
    })
    .then(function(stuff) {
      return _.extend(accumulator, stuff);
    });
  };

  /*
   * Register condtions
   * @params object
   * @returns promise of object
   */
  var recordCondition = function(condition, accumulator) {
    var condition = {
      asserter: [accumulator.user_id],
      code: {text: condition.condition},
      category: null, // "complaint | symptom | finding | diagnosis"
      clinicalStatus: condition.clinicalStatus, // "active | relapse | remission | resolved"
      verificationStatus: condition.verificationStatus, // "provisional | differential | confirmed | refuted | entered-in-error | unknown"
      severity: condition.severity == 'not-stated' ? null : condition.severity,
      onset: null,
      abatement: null,
      stage: null,
      evidence: [accumulator.source],
      bodySite: null,
      notes: '',
      event_id: accumulator.event.id,
      patient_id: accumulator.patient.id
    };
    return sys.models.Condition.create(condition);
  };

  /*
   * Register medication based on an APS referral
   * @params object
   * @returns promise of object
   */
  var recordTherapy = function(therapy, accumulator) {
    // need to extract to knowledge base:
  var therapies = {
    'fentanyl PCA': {
       medication: "morphine", route: "IV", method: "PCA", ongoing: true},
     'morphine PCA': {
       medication: "morphine", route: "IV", method: "PCA", ongoing: true},
     'single shot block': {
       medication: "", route: "block", method: "injection", ongoing: false},
     'block catheter': {
       medication: "local anaesthetic",
       route: "block",
       method: "block",
       ongoing: true},
     'spinal morphine': {
       medication: "morphine", route: "spinal", method: "", ongoing: false},
     'epidural': {
       medication: "local anaesthetic", route: "epidural", method: "", ongoing: true},
     'ketamine infusion': {
       medication: "ketamine", route: "IV", method: "infusion", ongoing: true},
  };

    // look up metadata
    var details = therapies[therapy.therapy] ||
      {medication: "unknown: " + therapy.therapy};
    var medication_statement = {
      // "active | completed | entered-in-error | intended"
      status: details.ongoing ? "active" : "completed",
      reasonForUse: { },
      start: {text: "in theatre"},
      end: details.ongoing ? null : {text: "in theatre"},
      note: therapy.notes,
      supportingInformation: { },
      medication: {text: details.medication},
      dosage: { },
      route: {text: details.route},
      site: { },
      method: {text: details.method},
      informationSource: ['user/'+ accumulator.user_id, accumulator.source],
      patient_id: accumulator.patient.id
    };
    return sys.models.MedicationStatement.create(medication_statement);
  };

  /*
   * Register an assessment event
   * @params object
   * - patient object (model)
   * - object (rec)
   * @returns promise of object
   * - patient object (model)
   * - event object (model)
   */
  var recordAssessment = function(params) {
    var event_rec = params.event_rec;
    event_rec.type = 'APS_assessment';
    return recordEvent(params.patient, event_rec);
  };

  /*
   * Register a discharge event
   * @params object
   * - patient object (model)
   * - object (rec)
   * @returns promise of object
   * - patient object (model)
   * - event object (model)
   */
  var recordDischarge = function(params) {
    var event_rec = params.event_rec;
    var accumulator = {};
    event_rec.type = 'APS_discharge';
    return recordEvent(params.patient, event_rec)
    .then(function(result) {
      accumulator = result;
      // debugging counter:
      accumulator.EpisodeOfCare_count = 0;
      // get episodes of care (there should be only one)
      return sys.models.EpisodeOfCare.findAll({where: {
        status: 'active',
        type: 'APS',
        patient_id: accumulator.patient.id
      }})
      // mark as complete
      .each(function(EpisodeOfCare) {
        console.log(EpisodeOfCare.get({plain: true}));
        accumulator.EpisodeOfCare_count++;
        EpisodeOfCare.status = 'finished';
        EpisodeOfCare.end = moment();
        return EpisodeOfCare.save();
      })
    })
    .then(function(EpisodeOfCares) {
      accumulator.EpisodeOfCares = EpisodeOfCares;
      accumulator.Flage_count = 0;
      // get flags (there should be only one)
      return sys.models.PatientFlag.findAll({where: {
        status: 'active',
        code: 'APS',
        patient_id: accumulator.patient.id
      }})
      // mark as complete
      .each(function(PatientFlag) {
        console.log(PatientFlag.get({plain: true}));
        accumulator.PatientFlag_count++;
        PatientFlag.status = 'inactive';
        PatientFlag.end = moment();
        return PatientFlag.save();
      })
    })
    .then(function(PatientFlags) {
      accumulator.PatientFlags = PatientFlags;
      return accumulator;
    });
    // then clear eocs
    // clear flag
  };

  /*
   * Create an event
   * @params object
   * - patient object (model)
   * - event(rec)
   * @returns promise of object
   * - patient object (model)
   * - event object (model)
   */
  var recordEvent = function(patient, event_rec) {
    var event_promise = sys.models.Event.create(event_rec).
    then(function(event) {
      config.log('creating event', event);
      return {
        patient: patient,
        event: event,
      }
    });
    return event_promise;
  };

  /*
   * Summarise the patient status
   * @params
   * - patient object (model)
   * - event object (model) (optional - the event triggering the review)
   * @returns promise of object
   */
  aps.patientStatus = function(aps_record) {
    config.log('getting status aps_record', aps_record);
    var patient = aps_record.patient;
    var result = {
      age: moment().diff(patient.dob, 'years'),
      dob: patient.dob,
      status: '?', // should eventulally say something "day1" or "slow wean"
      event: aps_record.event,
    };
    return Promise.resolve(result);
  };

  /**
    * Background task runner to find and run unactioned tasks
    *
    * returns immediately
    *
    * # new referrals:
    * - create episodes of care for referral (if none exist)
    * - scedule appointments
    * # ended episode of care:
    * - mark status = 'finished'
    * - update referral request(s) with status='completed'
    */
  aps.poll = function(params) {
    // list all APS reeferrals with accepted status:
    sys.models.ReferralRequest.findAll({where: {
      status: 'accepted',
      specialty: 'APS'
    }})
    .each(function(referral) {
      var accumulator = {};
      console.log('proccessing accepted APS referrals',
        referral.get.patient_id);
      return makeEpisodeOfCare(referral)
      .then(function(episodeOfCare) {
        accumulator.EpisodeOfCare = episodeOfCare;
        var apt_params = {
          ReferralRequest: referral,
          EpisodeOfCare: episodeOfCare,
        };
        return makeAppointment(apt_params);
      })
      .then(function(Appointment) {
        accumulator.Appointment = Appointment;
        var patientFlag = {
          category: "drug", // "diet|drug|lab|admin|contact|medical"
          status: "active", // "active|inactive|entered-in-error"
          start: moment(),
          end: null,
          encounter: null,
          author: null,
          code: 'APS',
          patient_id: referral.patient_id,
          text: 'Under care of APS'
        };
        return sys.models.PatientFlag.create(patientFlag);
      })
      .then(function(PatientFlag) {
        accumulator.PatientFlag = PatientFlag;
        return accumulator;
      });
    })
    .then(function(outcome) {
      console.log('polling outcome', outcome);
      console.log('poll run completed');
    });
    return Promise.resolve("polling");
  }




  /*
   * Create an EpisodeOfCare from a ReferralRequest
   * @params referral object
   * @returns promise of EpisodeOfCare object
   */
  var makeEpisodeOfCare = function(referral) {
    var result;
    var start = referral.priority == 'urgent'
      ? moment() : next_round();
    var eoc_rec = {
      status: 'active',
      statusHistory: null,
      type: 'APS',
      condition: null,
      start: start,
      end: null,
      referralRequest: [referral.id],
      careManager: null,
      careTeam: null,
      patient_id: referral.patient_id
    };
    return sys.models.EpisodeOfCare.create(eoc_rec);
  };

  /*
   * Create an appointment based on an EpisodeOfCare and a ReferralRequest
   * @params object
   * - referral
   * - EpisodeOfCare
   * @returns promise of Appointment
   */
  var makeAppointment = function(params) {
    var p = params;
    var result;
    var referral = p.ReferralRequest;
    var start = referral.priority == 'urgent'
      ? moment() : next_round();
    var apt_rec = {
      status: 'proposed',
      type: {code: 394882004, system: 'SNOMED', display: 'Pain management'},
      reason: null,
      priority: referral.priority == 'urgent' ? 4 : 5, // 1 is highest
      description: 'first APS visit',
      start: start,
      end: null,
      minutesDuration: null,
      slot: null,
      comment: null,
      participant: null,
      patient_id: referral.patient_id
    };
    return sys.models.Appointment.create(apt_rec);
  };

  return aps;

}

module.exports = Aps;

