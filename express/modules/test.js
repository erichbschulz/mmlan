"use strict";
var Promise = require('bluebird');
var _ = require('lodash');
var moment = require('moment');

var Test = function(config) {
  var sys = config.sys;
  var test = {};
  var aps = require('./aps')(config);
  var ontology_client = require('./ontology_client')(config);

  /*
   * Summary of tests
   * @returns promise of object
   */
  test.receive = function(params) {
    return Promise.resolve('no tests');
  };

  /*
   * Executed a test
   * @param string test identifier
   * @param params object
   * @returns promise of patient
   */
  test.run = function(test_id, params) {
    var test_runner = test[test_id];
    return (typeof test_runner == 'function')
      ? test_runner(params)
      : Promise.resolve('no tests');
  };

  test.ontology = function() {
    return ontology_client.test();
  };

  test.patient = function(params) {
    return aps.getPatient(params.session, params.query.patient_id);
  };

  test.discharge = function() {
    var discharge = {
      action: "assess",
      data: {
        "medication":[{"medication":"panadol"},{"medication":"PCA"}],
        // "review_plan":"review tomorrow",
        "review_plan":"discharge",
        "nausea":"none",
        "sedation":"none",
        "itch":"none",
        "motor_block":"none",
        "pain_at_rest":"1",
        "pain_on_movement":"2",
        "conditions":[],
        "notes":"continue endone for max 2 days"},
      patient_id: 51,
      user_id: 61234
    };
    return aps.receive(discharge);
  }

  test.refer = function() {
    var referral = {
      action: "refer",
      data: {
        notes: 'pca < 24/hours',
        therapies: [
        {therapy: "block catheter", notes: "rectus sheath" },
        {therapy: "fentanyl PCA", notes: "till eating" },
        ],
        conditions: [{
            condition: "sleep apnoea",
            severity: "severe",
            clinicalStatus: "active",
            verificationStatus: "confirmed"
          }, {
            condition: "diabetes",
            severity: "not-stated",
            clinicalStatus: "active",
            verificationStatus: "confirmed"
          }, {
            condition: "chronic pain",
            severity: "not-stated",
            clinicalStatus: "active",
            verificationStatus: "confirmed"
          }],
        review_plan: "review tomorrow"
      },
      patient_id: 51,
      user_id: 61234
    };
    return aps.receive(referral);
  }

  return test;

}

module.exports = Test;
