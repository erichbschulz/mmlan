"use strict";
/**
  Utilities to parse FHIR metadata and assist in preparation of
  sequelize schemas.
  */

var Promise = require('bluebird');
var S = require('sequelize');
var fs = require('fs');
var _ = require('lodash');
var path = require('path');
var writeFilePromise = Promise.promisify(fs.writeFile);
var readFilePromise = Promise.promisify(fs.readFile);
var statFilePromise = Promise.promisify(fs.stat);

module.exports = function(config) {

// this list is deprecated (should be derived from meta data)
var all = [
  "ValueSet-expand",
  "ValueSet-lookup",
  "ValueSet-validate-code",
  "ValueSet",
  "base",
  "base2",
  "Parameters",
  "Resource-validate",
  "Resource-meta",
  "Resource-meta-add",
  "Resource-meta-delete",
  "Resource",
  "DomainResource",
  "Account",
  "AllergyIntolerance",
  "Appointment",
  "AppointmentResponse",
  "AuditEvent",
  "Basic",
  "Binary",
  "BodySite",
  "Bundle",
  "CarePlan",
  "Claim",
  "ClaimResponse",
  "ClinicalImpression",
  "Communication",
  "CommunicationRequest",
  "Composition-document",
  "Composition",
  "ConceptMap-translate",
  "ConceptMap-closure",
  "ConceptMap",
  "Condition",
  "Conformance",
  "Contract",
  "Coverage",
  "DataElement",
  "DetectedIssue",
  "Device",
  "DeviceComponent",
  "DeviceMetric",
  "DeviceUseRequest",
  "DeviceUseStatement",
  "DiagnosticOrder",
  "DiagnosticReport",
  "DocumentManifest",
  "DocumentReference",
  "EligibilityRequest",
  "EligibilityResponse",
  "Encounter-everything",
  "Encounter",
  "EnrollmentRequest",
  "EnrollmentResponse",
  "EpisodeOfCare",
  "ExplanationOfBenefit",
  "FamilyMemberHistory",
  "Flag",
  "Goal",
  "Group",
  "HealthcareService",
  "ImagingObjectSelection",
  "ImagingStudy",
  "Immunization",
  "ImmunizationRecommendation",
  "ImplementationGuide",
  "List-find",
  "List",
  "Location",
  "Media",
  "Medication",
  "MedicationAdministration",
  "MedicationDispense",
  "MedicationOrder",
  "MedicationStatement",
  "MessageHeader-process-message",
  "MessageHeader",
  "NamingSystem",
  "NutritionOrder",
  "Observation",
  "OperationDefinition",
  "OperationOutcome",
  "Order",
  "OrderResponse",
  "Organization",
  "Patient-everything",
  "Patient",
  "PaymentNotice",
  "PaymentReconciliation",
  "Person",
  "Practitioner",
  "Procedure",
  "ProcedureRequest",
  "ProcessRequest",
  "ProcessResponse",
  "Provenance",
  "Questionnaire-populate",
  "Questionnaire",
  "QuestionnaireResponse",
  "ReferralRequest",
  "RelatedPerson",
  "RiskAssessment",
  "Schedule",
  "SearchParameter",
  "Slot",
  "Specimen",
  "StructureDefinition-questionnaire",
  "StructureDefinition",
  "Subscription",
  "Substance",
  "SupplyDelivery",
  "SupplyRequest",
  "TestScript",
  "VisionPrescription",
];
//all=['Substance'];

  // injected cofiguration variable
  var s = config.sequelize;
  var db = config.db;

  // main export object
  var meta = {};

  meta.all = function() {
    var result = {}; // default value on fail
    // from https://www.hl7.org/fhir/downloads.html
    var meta_json = require(path.join(__dirname,
        '../../meta/profiles-resources.json'));
    var resources = meta_json.entry;
    _.forEach(all, function(id) {
      var resource = (_.find(resources, function(r) {
        return r.resource.id == id;
      }) || {}).resource;
      if (resource) {
        var fields = (resource.snapshot||resource.differential || {element:[]})
          .element;
        result[id] = {
          lastUpdated: resource.meta.lastUpdated,
          items: fields.length
        };
      }
    })
    return Promise.resolve(result);
  };

  meta.meta = function(request_id) {
    var result = {}; // default value on fail
    // from https://www.hl7.org/fhir/downloads.html
    var meta_json = require(path.join(__dirname,
        '../../meta/profiles-resources.json'));
    var resources = meta_json.entry;
    var resource = (_.find(resources, function(r) {
      return r.resource.id == request_id;
    }) || {}).resource;
    if (resource) {
      var fields = (resource.snapshot || resource.differential || {})
        .element;
      result[request_id] = {
        resource: resource,
        lastUpdated: resource.meta.lastUpdated,
        fhirVersion: resource.fhirVersion,
        fields: fields,
        items: resources.length,
        resource: resource,
      }
    }
    return Promise.resolve(result);
  };

  /**
    * generate a sequelize schema based on the profiles-resources.json file
    * Writes a raw model definition to ../models.
    *
    * If a sequalize model already exists in ../models then returns both
    * concatentated as a string.
    */
  meta.schema = function(request_id) {
    // from https://www.hl7.org/fhir/downloads.html
    var meta_json = require(path.join(__dirname,
        '../../meta/profiles-resources.json'));
    var resources = meta_json.entry;
    var resource = (_.find(resources, function(r) {
      return r.resource.id == request_id;
    }) || {}).resource;
    if (!resource) {
      throw Error('no resource found called:' + request_id);
    }
    var fields = (resource.snapshot || resource.differential || {})
      .element;
    var schema = makeSchema(fields);
    var js = makeModelFile(request_id, schema);
    var file = path.join(__dirname, '../models/' + request_id + '.js');
    console.log('looking for file', file);
    return statFilePromise(file)
      .catch(function(err) {
        console.log('file not there', file);
        return false;
      })
      .then(function(stats) {
        if (stats && stats.isFile()) {
          console.log('file exists', file);
          return readFilePromise(file)
            .then(function(existing) {
              js = "///////////// existing:\n" + existing +
              "\n///////////// raw:\n" + js
            });
        }
        else {
          console.log('wrote file', file);
          return writeFilePromise(file, js);
        }
      })
      .then(function() { return js;});
  };

  function makeModelFile(model, schema) {
    return [
      '"use strict";',
      "module.exports = function(sequelize, S) {",
      "  var " + model + " = sequelize.define('" + model + "', {",
      schema,
      "  }, {",
      "    tableName: '" + _.camelCase(model) + "',",
      "    paranoid: true,",
      "    underscored: true,",
      "    classMethods: {",
      "      associate: function(models) {",
      "      // " + model + ".belongsTo(models.User);",
      "      }",
      "    }",
      "  });",
      "  return " + model + ";",
      "};"].join("\n");
  }

  function makeSchema(fields) {
    var it = "    "; // indent for prettiness
    var defs = [];
    _.forEach(fields, function(field) {
      var def = []; // collection of fields properties (as string)
      // split up the path because we only want the second level
      // (everthing else will go into JSONB)
      var path = field.path.split(".");
      var field_name = path[1];
      var types = [];
      if (path.length == 2) { // ie "entity.field"
        // loop over the types
        _.forEach(field.type, function(type, n) {
          types.push(type.code);
        });
        var code = _.uniq(types).join(';');
        if (field.max == 1 && types.length <= 1) {
          switch (code) {
            case 'base64Binary':
              def.push("type: S.TEXT");
              break;
            case 'boolean':
              def.push("type: S.BOOLEAN");
              break;
            case 'id':
              // practioners and patients will use their own campus wide keys
              if (_.includes(['Patient', 'Practitioner'], path[0])) {
                def.push("type: S.BIGINT.UNSIGNED");
              }
              else {
                def.push("type: S.UUID");
                def.push("defaultValue: S.UUIDV1");
              }
              def.push("primaryKey: true");
              break;
            case 'instant':
            case 'dateTime':
              def.push("type: S.DATE, defaultValue: S.NOW");
              break;
            case 'date':
              def.push("type: S.DATEONLY");
              break;
            case 'positiveInt':
              def.push("type: S.BIGINT.UNSIGNED");
              break;
            case 'unsignedInt':
              def.push("type: S.BIGINT.UNSIGNED");
              break;
            case 'string':
            case 'oid':
            case 'uri':
            case 'code':
              def.push("type: S.TEXT");
              break;
            default:
              def.push("type: S.JSONB");
          }
        }
        else { // some kind of complex thing use JSONB
          def.push("type: S.JSONB");
        }
        // all null if min == 0
        def.push("allowNull: " +
            ((field.min || code == 'id') ? 'false' : 'true'));
        def.push("comment: " + JSON.stringify(field.short));
        defs.push(field_name + ': {\n  ' +
            it + def.join(",\n  " + it) + '\n' +
            it + '}');
      }
      else {
        // console.log('skippng', path);
      }
    });
    return it + defs.join(",\n"+it);
  }

  return meta;


}
