"use strict";
var router = require('express').Router();
var _ = require('lodash');

module.exports = function(config) {

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/login', function(req, res) {
  var credentials = req.body;
  config.auth.login(req.session, credentials)
  .then(function(user) {
    console.log('user logged in', user);
    return res.json({
      success: true,
      name: user.name,
      roles: user.roles
      })
  })
  .catch(function(e) {
    console.error('error', e);
    if (e instanceof Error) {
      console.error(e.stack);
    }
    // obfuscate password
    if (e.options && e.options.form && e.options.form.password) {
      let password = e.options.form.password;
      e.options.form.password = "## " +
        (password.length > 5 ? "long" : "short") + " password obfuscated ##";
    }
    console.error('error logging in',
        _.pick(e, ['statusCode', 'statusMessage', 'body', 'options']));
    return res.status(200).json({ // if pass 401 then IIS swallows error!
      success: false,
      data: 'log in failed', // fixme
      error: e // fixme
    });
  });
});

return router;

}

