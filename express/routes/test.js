"use strict";
var router = require('express').Router();

/**
  * Routes for integration testing
  */
module.exports = function(config) {
  var test = require('../modules/test')(config);
  var dispatch = config.dispatch;

  // anonymous test summary inspector
  router.get('/status', function(req, res) {
    var promise = test.status();
    dispatch(promise, req, res);
  });

  // anonymous metadata parser
  router.get('/run/:test', function(req, res) {
    var promise = test.run(req.params.test, {
      session: req.session,
      query: req.query,
      });
    dispatch(promise, req, res);
  });

  return router;
}
