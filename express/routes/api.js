var router = require('express').Router();
var Promise = require('bluebird');
var _ = require('lodash');

module.exports = function(config) {

  // injected cofiguration variable
  var c = config;
  var db = c.db;
  var dispatch = config.dispatch;
  var aps = require('../modules/aps')(config);
  var sys = config.sys;
  var Patient = sys.models.Patient;
  var Event = sys.models.Event;

  router.post(c.route + '/patient/:patient_id/:action', function(req, res) {
    console.log('req.session.user', req.session.user);
    var data = {
      action: req.params.action,
      patient_id: req.params.patient_id,
      session: req.session,
//      user_id: req.session.user.id,
      data: req.body};
    var promise = aps.receive(data)
    .then(aps.patientStatus);
    dispatch(promise, req, res);
  });

  router.get(c.route + '/patient/:patient_id', function(req, res) {
    var promise = aps.getApsRecord(req.session, req.params.patient_id);
    dispatch(promise, req, res);
  });

  router.get(c.route + '/patients', function(req, res) {
    var promise = aps.getPatients(req.session);
    dispatch(promise, req, res);

  });

  // add the universal getter route
  _.forEach(sys.fhir_tables, function(model) {
    var route = c.route + '/' + model;
    console.log('adding get route', route);

    router.get(route, function(req, res) {
      console.log('req', req.query);
      var search_params = {
        // attributes: ['id', 'name', 'dob'],
        raw: true};
      if (req.query) {
        search_params.where = req.query;
      }
      dispatch(sys.models[model].findAll(search_params), req, res);
    });
  });


  //// test routes with basic sql /////
  router.post(c.route + '/item', function(req, res) {
    var item = {text: req.body.text, complete: false};
    var promise = db.run(
      "INSERT INTO items(text, complete) values($text, $complete)",
      {bind: item});
    dispatch(promise, req, res);
  });
  router.get(c.route + '/item/:urn', function(req, res) {
    var data = {id: req.params.urn};
      console.log('data', data);
    var promise = db.get(
      "SELECT * FROM items WHERE id=$id",
      {bind: data});
    dispatch(promise, req, res);
  });
  router.put(c.route + '/item/:urn', function(req, res) {
    var data = {
      id: req.params.urn,
      text: req.body.text,
      complete: req.body.complete
      };
      console.log('run');
    var promise = db.run(
      "UPDATE items SET text=$text, complete=$complete WHERE id=$id",
      {bind: data});
      console.log('run');
    dispatch(promise, req, res);
  });
  router.delete(c.route + '/item/:urn', function(req, res) {
    var id = req.params.urn;
    var promise = db.run("DELETE FROM items WHERE id=$id", {bind: {id: id}});
    dispatch(promise, req, res);
  });

  return router;

}

