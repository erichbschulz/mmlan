"use strict";
var router = require('express').Router();
var Promise = require('bluebird');
var _ = require('lodash');

/**
  * Routes for boostrapping and upgrading application
  */
module.exports = function(config) {
  var sys = config.sys;
  var meta = require('../modules/meta')(config);
  var aps = require('../modules/aps')(config);
  // standard result handlers (promise, req, res)
  var dispatch = config.dispatch;

  // anonymous inspector
  router.get('/bootstrap/status', function(req, res) {
    var promise = sys.status();
    dispatch(promise, req, res);
  });

  // anonymous poll trigger
  router.get('/aps/poll', function(req, res) {
    var promise = aps.poll();
    dispatch(promise, req, res);
  });

  // anonymous bootstrap
  router.get('/bootstrap/hardreset', function(req, res) {
    // do these steps in order:
    var promise = Promise.mapSeries([
      sys.hardPurge(),
      sys.reset(),
    ], _.identity);
    dispatch(promise, req, res);
  });

  // anonymous metadata summary
  router.get('/bootstrap/meta/all', function(req, res) {
    var promise = meta.all();
    dispatch(promise, req, res);
  });

  // anonymous metadata parser
  router.get('/bootstrap/meta/:resource', function(req, res) {
    var promise = meta.meta(req.params.resource);
    dispatch(promise, req, res);
  });

  // anonymous metadata parser
  router.get('/bootstrap/schema/:resource', function(req, res) {
    var promise = meta.schema(req.params.resource).then(function(data) {
      res.type('text/plain');
      return res.send(data);
    });
  });

  return router;
}
